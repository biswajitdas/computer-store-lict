
<%@ page import="com.cshop.bean.*,com.cshop.dao.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<div class="rc">
<%
	List<Category> list = ProductManagement.getCategories();
	request.setAttribute("list", list);
%>

	<c:forEach items="${list}" var="l">
		<h2>${l.getTitle()}</h2>
		<c:set var="cid" value="${l.getId()}"></c:set>


		<%
			int x = Integer.parseInt(pageContext.getAttribute("cid").toString());
				List<SubCategory> sublist = ProductManagement.getSubCategories(x);
				request.setAttribute("sublist", sublist);
		%>

		<ul>
			<c:forEach items="${sublist}" var="sl">
				<li><a href="products.jsp?subcat=${sl.getId()}">${sl.getTitle()}</a></li>
			</c:forEach>
		</ul>

	</c:forEach>


</div>


<!--End of rc-->

<div class="clear"></div>
</div>
<!-- End of content -->

<div class="footer">
	<ul>
		<li><a href="index.jsp">Home</a></li>
		<li><a href="products.jsp">Products</a></li>
		<li><a href="about.jsp">About Us</a></li>
		<li><a href="contact.jsp">Contact Us</a></li>
	</ul>
	<p>
		Developed By Biswajit Das & Sabrina Mobassirin
	</p>
</div>

</div>
<!-- End of wrapper -->
</body>
</html>