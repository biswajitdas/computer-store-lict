<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	%>


<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>


<!doctype html>
<html lang="en-US">
<head>
<meta charset="utf-8" />
<title>Computer Store</title>
<link rel="stylesheet" type="text/css" href="data/reset.css" />
<link rel="stylesheet" type="text/css" href="data/style.css" />
<script type="text/javascript" src="data/jquery-1.12.3.min.js"></script>

<script type="text/javascript">
	$(document).ready(
			function() {

				//$(".rc h2:first").addClass("active");
				//$(".rc ul:not(:first)").hide();
				$(".rc ul").hide();

				$(".rc h2").click(
						function() {
							$(this).next("ul").slideToggle("slow").siblings(
									"ul:visible").slideUp("slow");
							$(this).toggleClass("active");
							$(this).siblings("h2").removeClass("active");
						});

			});
</script>
</head>
<body>
	<div class="wrapper">
		<div class="header">
			<div class="nav">
				<ul>
					<li><a href="index.jsp">Home</a></li>
					<li><a href="products.jsp">Products</a></li>
					<li><a href="contact.jsp">Contact Us</a></li>
					<li><a href="update/signin.jsp">Sign In</a></li>
					<li><a href="signup.jsp">Sign Up</a></li>
					<li><a href="mycart.jsp">My Cart</a></li>
				</ul>
			</div>
		</div>
		<!-- End of Header -->