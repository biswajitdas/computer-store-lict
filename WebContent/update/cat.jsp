<jsp:include page="header.jsp" />
<%
	if (session.getAttribute("adminid") == null)
		response.sendRedirect("index.jsp");
%>

<%@ page
	import="com.cshop.dao.ProductManagement, com.cshop.bean.Category, java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%
	if (request.getParameter("del") != null) {
		int id = Integer.parseInt(request.getParameter("del"));
		
		boolean x = ProductManagement.removeById(id, "g022959_category");
		
		if (!x) {
			response.sendRedirect("cat.jsp?success=1");
		} else {
			response.sendRedirect("cat.jsp?success=0");
		}
	}


	if(request.getParameter("move")!=null){
		int db = Integer.parseInt(request.getParameter("db"));
		int id = Integer.parseInt(request.getParameter("id"));
		int x = ProductManagement.sort(db,id,request.getParameter("move"));

 		if(x!=0){
			response.sendRedirect("cat.jsp");
	} 
		
	}

	List<Category> list = ProductManagement.getCategories();
	request.setAttribute("list", list);
	
	int i =1;
%>


<div class="rc round">
	<h2>Categories</h2>
	<table class="info_table" width="730px" cellpadding="0" cellspacing="0">

		<tr>
			<td colspan="2"><c:choose>
					<c:when test="${param.success==1}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Category Deleted !** </span>
					</c:when>
					<c:when test="${param.success==0}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Failed to Delete !** </span>
					</c:when>
				</c:choose>
		<tr>
			<th width="11%">SL</th>
			<th width="71%">TITLE</th>
			<th width="18%">ACTIONS</th>
		</tr>




		<c:forEach items="${list}" var="l">

			<tr>
				<td><%=i++ %></td>
				<td style="text-align: left;">${l.getTitle() }</td>
				<td>
					<ul class="action_link">
						<li><a href="addcat.jsp?edit=${l.getId()}" class="edit"
							title="Edit">A</a></li>
						<li><a href="cat.jsp?del=${l.getId() }" class="del"
							onclick="return confirm('Are you sure ?');" title="Delete">A</a></li>
						<li><a href="cat.jsp?db=1&amp;id=${l.getId()}&amp;move=up" class="up" title="Up">A</a></li>
						<li><a href="cat.jsp?db=1&amp;id=${l.getId()}&amp;move=down" class="down" title="Down">A</a></li>
					</ul>
				</td>
			</tr>

		</c:forEach>


	</table>
</div>
<div class="clear"></div>
</div>
</div>
</body>
</html>