<jsp:include page="header.jsp" />

<%@ page import="com.cshop.bean.*,com.cshop.dao.*,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<%
	if (session.getAttribute("adminid") == null)
		response.sendRedirect("index.jsp");

	if (request.getParameter("status") == null) {
		List<Order> list = ProductManagement.getOrderes(-1);

		request.setAttribute("list", list);
	}

  int status = -1;
 	if (request.getParameter("status") != null) {
		status = Integer.parseInt(request.getParameter("status"));
		List<Order> list = ProductManagement.getOrderes(-1, status);
		request.setAttribute("list", list);
	}

	if (request.getParameter("id") != null) {
		int id = Integer.parseInt(request.getParameter("id"));
		int x = ProductManagement.approve(id, 1);

		if (x != 0)
			response.sendRedirect("approval.jsp?success=1");
		else
			response.sendRedirect("approval.jsp?success=0");
	} 
	
	if(request.getParameter("did")!=null) {
		int did = Integer.parseInt(request.getParameter("did"));
		int x = ProductManagement.approve(did, -1);
		if (x != 0)
			response.sendRedirect("approval.jsp?success=1");
		else
			response.sendRedirect("approval.jsp?success=0");
	} 
 
	int i = 1;
%>


<div class="rc round">
	<h2>Order Approval</h2>

	<table class="info_table" width="730px" cellpadding="0" cellspacing="0">

		<tr>
			<td colspan="7">
			<c:choose>
			<c:when test="${param.success==1}">
					<span
						style="color: red; display: block; background-color: yellow; text-align: center">
						**Operation executed Successfully !** </span>
				</c:when>
				<c:when test="${param.success==0}">
				<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**Operation failed !** </span>
				</c:when>
			</c:choose>
				</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<form>
					<select id="id" onChange="check();">
						<option value="-1">All</option>
						<option <%=(status == 1) ? "selected" : ""%> value="1">Accepted</option>
						<option <%=(status == 0) ? "selected" : ""%> value="0">Pending</option>
					</select>
				</form>
			</td>
		</tr>
		<tr>
			<th width="10%">Serial</th>
			<th width="20%">Customer Name</th>
			<th width="15%">Title</th>
			<th width="15%">Quantity</th>
			<th width="15%">Price</th>
			<th width="10%">Accepted</th>
			<th width="15%">Action</th>
		</tr>

		<c:forEach items="${list}" var="l">
			<tr>
				<td><%=i++%></td>

				<c:set value="${l.getPid()}" var="pid" />
				<c:set value="${l.getCid()}" var="cid" />

				<%
					int pid = Integer.parseInt(pageContext.getAttribute("pid").toString());
						Product product = ProductManagement.getProduct(pid);

						int cid = Integer.parseInt(pageContext.getAttribute("cid").toString());
						User user = UserManagement.getUser(cid);
				%>

				<td><%=user.getName()%></td>
				<td><%=product.getTitle()%></td>
				<td>${l.getQuantity()}</td>
				<td>${l.getPrice()}</td>
				<td>${l.getAccepted()==1?"Accepted":"Pending"}</td>
				<td><c:choose>
						<c:when test="${l.getAccepted()!=1}">
							<a class="abutton"
								style="text-decoration: none; display: block; background-color: #3FA9F5; color: #fff; border-radius: 5px; padding: 5px"
								href="approval.jsp?id=${l.getId()}">Approve</a>
						</c:when>

						<c:when test="${l.getAccepted()==1}">
							<a class="abutton"
								style="text-decoration: none; display: block; background-color: #3FA9F5; border-radius: 5px; color: #fff; padding: 5px"
								href="approval.jsp?did=${l.getId()}">Discard</a>
						</c:when>
					</c:choose></td>
			</tr>

		</c:forEach>




	</table>


</div>
<div class="clear"></div>
</div>
</div>
<script type="text/javascript">
	function check() {
		var id = document.getElementById('id').value;
		if (id == -1)
			window.location = 'approval.jsp';
		else
			window.location = 'approval.jsp?status=' + id;
	}
</script>
</body>
</html>