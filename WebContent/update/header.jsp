<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<title>Computers Store Admin Panel</title>
<link rel="stylesheet" type="text/css" href="data/style.css" />
<link rel="stylesheet" href="redactor/css/redactor.css" />
<script type="text/javascript" src="data/jquery-1.7.min.js"></script>

<script src="redactor/redactor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#redactor').redactor({
			focus : true
		});
	});

	$(document).ready(function() {
		$("#msg").animate({
			opacity : 'hide'
		}, 5000);
	});
</script>

</head>



<body>
	<div class="wrapper round">
		<div class="header roundbottom">
			<h2>Computers Store</h2>
			<div class="topnav">
				<ul>
					<li><a href="admin.jsp">Dashboard</a></li>
					<li><a href="../index.jsp" target="_blank">Back to website</a></li>
					<li><a href="logout.jsp?type=0">Log Out</a></li>
				</ul>
			</div>
		</div>
		<div class="content round boxshadow">
			<div class="lc">
				<div class="sidenav round">
					<h2>Manage Products</h2>
					<ul>
						<li><a href="cat.jsp">Categories</a></li>
						<li><a href="addcat.jsp">Add A Category</a></li>
						<li><a href="subcat.jsp">Sub Categories</a></li>
						<li><a href="addsubcat.jsp">Add A Sub Category</a></li>
						<li><a href="products.jsp">Show || Edit Products</a></li>
						<li><a href="addproducts.jsp">Add A Product</a></li>
					</ul>
				</div>

				<div class="sidenav round">
					<h2>Manage Orders</h2>
					<ul>
						<li><a href="approval.jsp">Order Approval</a></li>
						<!-- <li><a href="vendor.jsp">Order to Vendor</a></li> -->
					</ul>
				</div>

				<div class="sidenav round">
					<h2>Account</h2>
					<ul>
						<li><a href="cp.jsp">Change Password</a></li>
					</ul>
				</div>

			</div>