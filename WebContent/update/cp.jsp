<jsp:include page="header.jsp"></jsp:include>

<%@ page
	import="com.cshop.dao.UserManagement,com.cshop.bean.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="rc round">
	<h2>Change Password</h2>

	<%
		if (session.getAttribute("adminid") == null)
			response.sendRedirect("index.jsp");
	%>

	<jsp:useBean id="bean" class="com.cshop.bean.User"></jsp:useBean>
	<jsp:setProperty property="*" name="bean" />

	<jsp:setProperty property="id" name="bean"
		value="${sessionScope.adminid}" />

	<%
		int id = 0;
		if (session.getAttribute("adminid") != null) {
			id = Integer.parseInt(session.getAttribute("adminid").toString());

			User user = UserManagement.getUser(id);

			request.setAttribute("user", user);
		}
	%>

	<form class="form_area" action="" method="POST">
		<table cellpadding="0" cellspacing="0" width="730px"
			class="form_table">
			<tr>
				<td colspan="2"><c:choose>
						<c:when test="${param.submit!=null}">
							<%
								int flag = UserManagement.updatePassword(id, request.getParameter("password"),
												request.getParameter("newpassword"));

										if (flag != 0) {
											response.sendRedirect("cp.jsp?success=1");
										} else {
											response.sendRedirect("cp.jsp?success=0");
										}
							%>
						</c:when>

						<c:when test="${param.success==1}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**Password Changed Successfully !** </span>
						</c:when>

						<c:when test="${param.success==0}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**Wrong old password ** </span>
						</c:when>

					</c:choose></td>
			</tr>
			<tr>
				<td width="96">Username</td>
				<td width="632"><input type="text" name="un"
					disabled="disabled" value="${user.getUsername()}" /></td>
			</tr>
			<tr>
				<td>Old Password</td>
				<td><input type="password" name="password" /></td>
			</tr>
			<tr>
				<td>New Password</td>
				<td><input type="password" name="newpassword" /></td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td><input class="button" name="submit" value="Change Password"
					type="submit" /> <input class="button" type="reset" /></td>
			</tr>

		</table>
	</form>

</div>
<div class="clear"></div>
</div>
</div>
</body>
</html>