<jsp:include page="header.jsp" />
<%@ page
	import="com.cshop.dao.ProductManagement,com.cshop.bean.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%
	if (session.getAttribute("adminid") == null)
		response.sendRedirect("index.jsp");

	if (request.getParameter("del") != null) {
		int id = Integer.parseInt(request.getParameter("del"));

		boolean x = ProductManagement.removeById(id, "g022959_subcat");

		if (!x) {
			response.sendRedirect("addsubcat.jsp?delsuccess=1");
		} else {
			response.sendRedirect("addsubcat.jsp?delsuccess=0");
		}
	}

	if (request.getParameter("move") != null) {
		int db = Integer.parseInt(request.getParameter("db"));
		int id = Integer.parseInt(request.getParameter("id"));
		int x = ProductManagement.sort(db, id, request.getParameter("move"));

		if (x != 0) {
			response.sendRedirect("addsubcat.jsp");
		}

	}

	if (request.getParameter("edit") != null) {
		int id = Integer.parseInt(request.getParameter("edit"));
		SubCategory subcat = ProductManagement.getSubCategory(id);
		request.setAttribute("subcat", subcat);
	}

	if (request.getParameter("id") != null & request.getParameter("submit") != null) {
		int id = Integer.parseInt(request.getParameter("id"));
		int cat = Integer.parseInt(request.getParameter("category"));

		int x = ProductManagement.updateSubCategory(id, cat, request.getParameter("title"));

		if (x != 0) {
			response.sendRedirect("addsubcat.jsp?success=2");
		} else {
			response.sendRedirect("addsubcat.jsp?success=0");
		}
	}

	if (request.getParameter("id") == null & request.getParameter("submit") != null) {

		int cat = Integer.parseInt(request.getParameter("category"));

		int x = ProductManagement.insertSubCategory(cat, request.getParameter("title"));

		if (x != 0) {
			response.sendRedirect("addsubcat.jsp?success=1");
		} else {
			response.sendRedirect("addsubcat.jsp?success=0");
		}

	}

	List<Category> categories = ProductManagement.getCategories();
	request.setAttribute("categories", categories);

	int i = 1;
	int cid = -1;
%>

<c:choose>


	<c:when test="${param.cat!=null}">
		<%
			cid = Integer.parseInt(request.getParameter("cat"));

					List<SubCategory> list = ProductManagement.getSubCategories(cid);

					request.setAttribute("subcategories", list);
					request.setAttribute("cid", cid);
		%>
	</c:when>

	<c:when test="${param.cat==null}">
		<%
			List<SubCategory> list = ProductManagement.getSubCategories(-1);

					request.setAttribute("subcategories", list);
		%>
	</c:when>


</c:choose>

<div class="rc round">
	<h2>Add Sub category</h2>
	<form class="form_area" action="addsubcat.jsp" method="post">
		<table cellpadding="0" cellspacing="0" width="730px"
			class="form_table">
			<tr>
				<td colspan="2"><c:choose>
						<c:when test="${param.success==1}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**SubCategory inserted Successfully !** </span>
						</c:when>

						<c:when test="${param.success==0}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**Sorry Try again ** </span>
						</c:when>

						<c:when test="${param.success==2}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**SubCategory edited successfully ** </span>
						</c:when>


					</c:choose></td>
			</tr>
			<tr>
				<td>Select Category</td>


				<td><select name="category">
						<option value="">Select Category</option>
						<c:forEach items="${categories}" var="c">
							<option ${c.getId()==subcat.getCategory()?"selected":"" }
								value="${c.getId()}">${c.getTitle()}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td width="96">Sub-Cat Title</td>
				<c:if test="${param.edit!=null}">
					<input type="hidden" name="id" value="${param.edit}" />
				</c:if>
				<td width="632"><input type="text" name="title"
					value="${subcat.getTitle()}" autofocus="autofocus" /></td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td><input class="button" name="submit" value="Submit"
					type="submit" /> <input class="button" type="reset" /></td>
			</tr>
		</table>
	</form>

	<table class="info_table" width="730px" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="4"><c:choose>
					<c:when test="${param.delsuccess==1}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**SubCategory Deleted !** </span>
					</c:when>
					<c:when test="${param.delsuccess==0}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Failed to Delete !** </span>
					</c:when>
				</c:choose>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<form>

					<select name="cat" id="cat" onChange="getCatagory();">
						<option value="">Show ALL</option>

						<c:forEach items="${categories}" var="c">

							<c:set value="${c.getId()}" var="tid" />

							<%
								int tid = Integer.parseInt(pageContext.getAttribute("tid").toString());
							%>

							<option <%=(tid==cid)?"selected":"" %> value="${c.getId()}">${c.getTitle()}</option>
						</c:forEach>


					</select>
				</form>
			</td>
		</tr>

		<tr>
			<th width="11%">SL</th>
			<th width="36%">Sub Category</th>
			<th width="35%">Category</th>
			<th width="18%">ACTIONS</th>
		</tr>




		<c:forEach items="${subcategories}" var="subcat">

			<c:set value="${subcat.getCategory()}" var="catid"></c:set>

			<%
				int catid = Integer.parseInt(pageContext.getAttribute("catid").toString());

					Category cat = ProductManagement.getCategory(catid);

					request.setAttribute("cat", cat);
			%>

			<tr>
				<td><%=i++%></td>
				<td style="text-align: left;">${subcat.getTitle()}</td>
				<td style="text-align: left;">${cat.getTitle()}</td>
				<td>
					<ul class="action_link">
						<li><a href="addsubcat.jsp?edit=${subcat.getId()}"
							class="edit" title="Edit">A</a></li>
						<li><a href="addsubcat.jsp?del=${subcat.getId()}" class="del"
							onclick="return confirm('Are you sure ?');" title="Delete">A</a></li>
						<li><a
							href="addsubcat.jsp?db=3&amp;id=${subcat.getId()}&amp;move=up"
							class="up" title="Up">A</a></li>
						<li><a
							href="addsubcat.jsp?db=3&amp;id=${subcat.getId()}&amp;move=down"
							class="down" title="Down">A</a></li>
					</ul>
				</td>
			</tr>
		</c:forEach>




	</table>
</div>
<div class="clear"></div>
</div>
</div>
<script type="text/javascript">
	function getCatagory() {
		var catid = document.getElementById('cat').value;

		if (catid) {
			window.location = 'addsubcat.jsp?cat=' + catid;
		} else {
			window.location = 'addsubcat.jsp';
		}
	}
</script>
</body>
</html>