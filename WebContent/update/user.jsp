<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.cshop.dao.ProductManagement,com.cshop.bean.*, java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<title>Computers Store Admin Panel</title>
<link rel="stylesheet" type="text/css" href="data/style.css" />
<link rel="stylesheet" href="redactor/css/redactor.css" />
<script type="text/javascript" src="data/jquery-1.7.min.js"></script>

<script src="redactor/redactor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#redactor').redactor({
			focus : true
		});
	});

	$(document).ready(function() {
		$("#msg").animate({
			opacity : 'hide'
		}, 5000);
	});
</script>

</head>
<body>
	<div class="wrapper round">
		<div class="header roundbottom">
			<h2>Computers Store</h2>
			<div class="topnav">
				<ul>
					<li><a href="../index.jsp" target="_blank">Back to website</a></li>
					<li><a href="logout.jsp?type=1">Log Out</a></li>
				</ul>
			</div>
		</div>
		<div class="content round boxshadow">
			<div class="lc">
				<div class="sidenav round">
					<h2>Manage Account</h2>
					<ul>
						<li><a href="user.jsp">My Orders</a></li>
						<li><a href="changepass.jsp">Change Password</a></li>
					</ul>
				</div>
			</div>

			<%
				int status = -1;

				if (session.getAttribute("id") == null)
					response.sendRedirect("signin.jsp");
				else if (request.getParameter("status") != null) {

					int cid = Integer.parseInt(session.getAttribute("id").toString());
					status = Integer.parseInt(request.getParameter("status").toString());

					List<Order> list = ProductManagement.getOrderes(cid, status);
					request.setAttribute("list", list);

				} else {
					List<Order> list = ProductManagement
							.getOrderes(Integer.parseInt(session.getAttribute("id").toString()));
					request.setAttribute("list", list);
				}

				if (request.getParameter("move") != null) {
					boolean flag = request.getParameter("move").contains("up");
					int id = Integer.parseInt(request.getParameter("id").toString());
					int pid = Integer.parseInt(request.getParameter("pid").toString());

					if (flag) {
						ProductManagement.increaseQuantity(id, pid);
					} else {
						ProductManagement.decreaseQuantity(id, pid);
					}
					response.sendRedirect("user.jsp");
				}

				if (request.getParameter("del") != null) {
					int id = Integer.parseInt(request.getParameter("del"));

					//ProductManagement.removeOrder(id);
					ProductManagement.removeById(id, "g022959_orders");
					response.sendRedirect("user.jsp");
				}

				int i = 1;
				int totalPrice = 0;
			%>

			<div class="rc round">
				<h2>My Orders</h2>

				<table class="info_table" width="730px" cellpadding="0"
					cellspacing="0">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<form>
								<select id="id" onChange="check();">
									<option value="">Select</option>
									<option <%=(status == 1) ? "selected" : ""%> value="1">Accepted</option>
									<option <%=(status == 0) ? "selected" : ""%> value="0">Pending</option>
								</select>
							</form>
						</td>
					</tr>
					<tr>
						<th width="10%">Serial</th>
						<th width="25%">Title</th>
						<th width="20%">Quantity</th>
						<th width="20%">Price</th>
						<th width="10%">Accepted</th>
						<th width="15%">Action</th>
					</tr>


					<c:forEach items="${list}" var="l">
						<c:set var="pid" value="${l.getPid()}"></c:set>
						<c:set var="price" value="${l.getPrice()}"></c:set>

						<%
							int pid = Integer.parseInt(pageContext.getAttribute("pid").toString());
								Product product = ProductManagement.getProduct(pid);

								totalPrice += Integer.parseInt(pageContext.getAttribute("price").toString());
								request.setAttribute("pd", product);
						%>

						<tr>
							<td><%=i++%></td>

							<td>${pd.getTitle()}</td>
							<td>${l.getQuantity()}</td>
							<td>$${l.getPrice()}</td>
							<td>${l.getAccepted()==1?"accepted":"pending"}</td>
							<td>
								<ul class="action_link">
									<li><a
										href="user.jsp?id=${l.getId()}&pid=${pd.getId()}&move=up"
										class="up" title="Up">A</a></li>
									<li><a
										href="user.jsp?id=${l.getId()}&pid=${pd.getId()}&move=down"
										class="down" title="Down">A</a></li>
									<li><a href="user.jsp?del=${l.getId()}" class="del"
										onclick="return confirm('Are you sure ?');" title="Delete">A</a>
									</li>
								</ul>
							</td>
						</tr>

					</c:forEach>

					<tr>
						<td></td>
						<td></td>
						<td>Total Price :</td>
						<td>$<%=totalPrice%></td>
						<td></td>
					</tr>

				</table>


			</div>
			<div class="clear"></div>
		</div>
	</div>
	<script type="text/javascript">
		function check() {
			var id = document.getElementById('id').value;
			window.location = 'user.jsp?status=' + id;
		}
	</script>
</body>
</html>