<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.cshop.dao.UserManagement"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Login Page</title>

<style type="text/css">
* {
	margin: 0;
	padding: 0;
}

body {
	font-family: Georgia, serif;
	background: url(images/login-page.jpg) top center no-repeat #c4c4c4;
	color: #3a3a3a;
}

.clear {
	clear: both;
}

form {
	width: 406px;
	margin: 170px auto 0;
}

form p {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF3300;
	display: block;
	text-align: center;
	padding: 2px 0;
}

legend {
	display: none;
}

fieldset {
	border: 0;
}

label {
	width: 115px;
	text-align: right;
	float: left;
	margin: 0 10px 0 0;
	padding: 9px 0 0 0;
	font-size: 16px;
}

input {
	width: 220px;
	display: block;
	padding: 4px;
	margin: 0 0 10px 0;
	font-size: 18px;
	color: #3a3a3a;
	font-family: Georgia, serif;
}

input[type=checkbox] {
	width: 20px;
	margin: 0;
	display: inline-block;
}

.button {
	background: url(images/button-bg.png) repeat-x top center;
	border: 1px solid #999;
	-moz-border-radius: 5px;
	padding: 5px;
	color: black;
	font-weight: bold;
	-webkit-border-radius: 5px;
	font-size: 13px;
	width: 70px;
}

.button:hover {
	background: white;
	color: black;
}
</style>
</head>

<body>

	<form action="index.jsp" method="post" id="login-form">
		<fieldset>

			<legend>Log in</legend>

			<%
				if (session.getAttribute("adminid") != null) {
					response.sendRedirect("admin.jsp");
				}
			%>

			<jsp:useBean id="bean" class="com.cshop.bean.User"></jsp:useBean>
			<jsp:setProperty property="*" name="bean" />
			<c:choose>

				<c:when test="${param.submit!=null}">
					<%
						int id = UserManagement.signin(bean, 0);

								if (id != 0) {
									session.setAttribute("adminid", id);
									response.sendRedirect("admin.jsp");
								} else {
									response.sendRedirect("index.jsp?success=0");
								}
					%>

				</c:when>

				<c:when test="${param.success==0}">
					<p>Failed to signin. Try again!</p>
				</c:when>
				
				<c:when test="${param.logout==1}">
					<p>Logged out successfully!</p>
				</c:when>

			</c:choose>


			<label for="login-username">Username</label> <input type="text"
				name="username" id="login-username" class="round"
				autofocus="autofocus" />
			<div class="clear"></div>

			<label for="login-password">Password</label> <input type="password"
				name="password" id="login-password" class="round" />
			<div class="clear"></div>

			<br /> <input type="submit" style="margin: -20px 0 0 287px;"
				class="button" name="submit" value="Log in" />
		</fieldset>
	</form>

</body>

</html>