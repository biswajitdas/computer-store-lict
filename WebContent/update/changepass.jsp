<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.cshop.dao.UserManagement,com.cshop.bean.*, java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
<meta charset="UTF-8">
<title>Computers Store Admin Panel</title>
<link rel="stylesheet" type="text/css" href="data/style.css" />
<link rel="stylesheet" href="redactor/css/redactor.css" />
<script type="text/javascript" src="data/jquery-1.7.min.js"></script>

<script src="redactor/redactor.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#redactor').redactor({
			focus : true
		});
	});

	$(document).ready(function() {
		$("#msg").animate({
			opacity : 'hide'
		}, 5000);
	});
</script>

</head>
<body>
	<div class="wrapper round">
		<div class="header roundbottom">
			<h2>Computers Store</h2>
			<div class="topnav">
				<ul>
					<li><a href="../index.jsp" target="_blank">Back to website</a></li>
					<li><a href="logout.jsp?type=1">Log Out</a></li>
				</ul>
			</div>
		</div>
		<div class="content round boxshadow">
			<div class="lc">
				<div class="sidenav round">
					<h2>Manage Account</h2>
					<ul>
						<li><a href="user.jsp">My Orders</a></li>
						<li><a href="changepass.jsp">Change Password</a></li>
					</ul>
				</div>
			</div>
			<div class="rc round">
				<h2>Change Password</h2>
				
				<%
				if (session.getAttribute("id") == null)
					response.sendRedirect("signin.jsp");
				%>

				<jsp:useBean id="bean" class="com.cshop.bean.User"></jsp:useBean>
				<jsp:setProperty property="*" name="bean" />

				<jsp:setProperty property="id" name="bean"
					value="${sessionScope.id}" />

				<form class="form_area" action="" method="POST">
					<table cellpadding="0" cellspacing="0" width="730px"
						class="form_table">
						<tr>
							<td colspan="2"><c:choose>
									<c:when test="${param.submit!=null}">
										<%
																			
							int id = Integer.parseInt(session.getAttribute("id").toString());
							int flag = UserManagement.updatePassword(id,request.getParameter("password"),request.getParameter("newpassword"));
						
							if(flag!=0){
								response.sendRedirect("changepass.jsp?success=1");
							}
							else{
								response.sendRedirect("changepass.jsp?success=0");
							}
							
						%>
									</c:when>

									<c:when test="${param.success==1}">
									<span style="color:red; display:block; background-color:yellow; text-align:center">
									**Password Changed Successfully !** </span>
									</c:when>

									<c:when test="${param.success==0}">
									<span style="color:red; display:block; background-color:yellow; text-align:center">
									**Wrong old password ** </span>
									</c:when>

								</c:choose></td>
						</tr>
						<tr>
							<td width="96">Username</td>
							<td width="632"><input type="text" name="un"
								disabled="disabled" value="user" /></td>
						</tr>
						<tr>
							<td>Old Password</td>
							<td><input type="password" name="password" /></td>
						</tr>
						<tr>
							<td>New Password</td>
							<td><input type="password" name="newpassword" /></td>
						</tr>

						<tr>
							<td>&nbsp;</td>
							<td><input class="button" name="submit"
								value="Change Password" type="submit" /> <input class="button"
								type="reset" /></td>
						</tr>

					</table>
				</form>

			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>