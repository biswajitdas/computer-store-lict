
<jsp:include page="header.jsp" />

<%@ page import="com.cshop.bean.*,com.cshop.dao.*,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
	if (session.getAttribute("adminid") == null)
		response.sendRedirect("index.jsp");
%>


<%-- <jsp:useBean id="bean" class="com.cshop.bean.Product"></jsp:useBean>
<jsp:setProperty property="*" name="bean" />
<c:choose>

	<c:when test="${param.submit!=null}">
		<%
			int x = ProductManagement.addProduct(bean);

					if (x != 0) {
						response.sendRedirect("addproducts.jsp?success=1");
					} else {
						response.sendRedirect("addproducts.jsp?success=0");
					}
		%>

	</c:when>

</c:choose> --%>
<%
	int catid = (request.getParameter("cat") != null) ? Integer.parseInt(request.getParameter("cat")) : -1;
	int subcatid = (request.getParameter("subcat") != null)
			? Integer.parseInt(request.getParameter("subcat"))
			: -1;
	int editid = (request.getParameter("edit") != null) ? Integer.parseInt(request.getParameter("edit")) : -1;

	if (catid == -1 && subcatid == -1) {

		List<Product> products = ProductManagement.getProducts(-1);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(-1);
		request.setAttribute("subcategories", subcategories);

	} else if (catid != -1 && subcatid == -1) {

		List<Product> products = ProductManagement.getProductsByCategory(catid, -1);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(catid);
		request.setAttribute("subcategories", subcategories);

	} else if (catid == -1 && subcatid != -1) {

		List<Product> products = ProductManagement.getProductsByCategory(-1, subcatid);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(-1);
		request.setAttribute("subcategories", subcategories);

	} else if (catid != -1 && subcatid != -1) {

		List<Product> products = ProductManagement.getProductsByCategory(catid, subcatid);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(catid);
		request.setAttribute("subcategories", subcategories);

	}

	if (request.getParameter("move") != null) {
		int id = Integer.parseInt(request.getParameter("id"));

		int x = ProductManagement.sort(2, id, request.getParameter("move"));

		if (x != 0)
			response.sendRedirect("addproducts.jsp");

	}

	if (request.getParameter("del") != null) {
		int id = Integer.parseInt(request.getParameter("del"));

		boolean x = ProductManagement.removeById(id, "g022959_products");

		if (!x)
			response.sendRedirect("addproducts.jsp?delsuccess=1");
		else
			response.sendRedirect("addproducts.jsp?delsuccess=0");
	}

	if (editid != -1) {

		Product product = ProductManagement.getProduct(editid);
		request.setAttribute("product", product);
	}

	List<Category> categories = ProductManagement.getCategories();
	request.setAttribute("categories", categories);

	request.setAttribute("catid", catid);
	request.setAttribute("subcatid", subcatid);
	request.setAttribute("editid", editid);
	
	int i = 1;
%>


<div class="rc round">
	<h2>Add Product</h2>


	<c:choose>
		<c:when test="${param.success==1}">
			<span
				style="color: red; display: block; background-color: yellow; text-align: center">
				**Product inserted Successfully !** </span>
		</c:when>

		<c:when test="${param.success==0}">
			<span
				style="color: red; display: block; background-color: yellow; text-align: center">
				**Sorry Try again ** </span>
		</c:when>

		<c:when test="${param.success==2}">
			<span
				style="color: red; display: block; background-color: yellow; text-align: center">
				**Product edited successfully ** </span>
		</c:when>

	</c:choose>

	<form class="form_area" action="uploadServlet" method="post"
		enctype="multipart/form-data">

		<input type="hidden" name="id" id="h_id" value="${product.getId()!=null?product.getId():0}" />
		
		<table cellpadding="0" cellspacing="0" width="730px"
			class="form_table">
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td>Select Category</td>
				<td><select name="category" id="category"
					onchange="getSubCat()">
						<option value="">Select Category</option>
						<c:forEach items="${categories}" var="cat">

							<%--						<option ${cat.getId()==catid?"selected":""}
								value="${cat.getId()}">${cat.getTitle()}</option> --%>

							<option
								${(((catid!=-1) && (editid!=-1)) || (catid!=-1))?((cat.getId()==catid)?"selected":""):(edit!=-1)?(cat.getId()==product.getCategory()?"selected":""):""}
								value="${cat.getId()}">${cat.getTitle()}</option>

						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td>Select Sub Category</td>
				<td><select name="subcat">
						<option value="">Select Subcat</option>
						<c:forEach items="${subcategories}" var="subcat">

							<%-- 							<option ${subcat.getId()==subcatid?"selected":""}
								value="${subcat.getId()}">${subcat.getTitle()}</option> --%>

							<option
								${(((subcatid!=-1) && (editid!=-1)) || (subcatid!=-1))?((subcat.getId()==subcatid)?"selected":""):(edit!=-1)?(subcat.getId()==product.getSubcat()?"selected":""):""}
								value="${subcat.getId()}">${subcat.getTitle()}</option>

						</c:forEach>
				</select></td>
			</tr>

			<tr>
				<td width="104">Product Title</td>
				<td width="624"><input type="text" name="title"
					value="${product.getTitle()}" /></td>
			</tr>

			<tr>
				<td>Sale Price</td>
				<td><input type="text" name="sprice"
					value="${product.getSprice()}" /></td>
			</tr>

			<tr>
				<td>Quantity</td>
				<td><input type="number" name="quantity"
					value="${product.getQuantity()}" min="1" /></td>
			</tr>

			<tr>
				<td>Discount</td>
				<td><input type="number" name="discount"
					value="${product.getDiscount()}" min="0" /></td>
			</tr>

			<tr>
				<td>Discount type</td>
				<td><select name="distype">
						<option ${product.getDistype()==1?"selected":""} value="1">Parcent</option>
						<option ${product.getDistype()==2?"selected":""} value="2">Dollar</option>
				</select></td>
			</tr>


			<tr>
				<td>Make Feature</td>
				<td><input type="radio" name="feature" id="mf1" value="1"
					${product.getFeature()==1?"checked":""} checked="checked" /> <label
					for="mf1">YES </label> <input type="radio" name="feature" id="mf2"
					value="0" ${product.getFeature()==0?"checked":""} /> <label
					for="mf2">NO </label></td>
			</tr>


			<tr>
				<td>Upload image<br /> 160px * 160px
				</td>
				<td><input type="file" name="image" /></td>
			</tr>

			<tr>
				<td>Product's Details</td>
				<td></td>
			</tr>

			<tr>
				<td colspan="2"><textarea name="content" id="redactor">${product.getContent()}</textarea>
				</td>
			</tr>


			<tr>
				<td>&nbsp;</td>
				<td><input class="button" name="submit" value="Submit"
					type="submit" /> <input class="button" name="reset" value="Reset"
					type="Reset" /></td>

			</tr>

		</table>
	</form>

	<table id="list" class="info_table" width="730px" cellpadding="0"
		cellspacing="0">
		<tr>
			<td colspan="7"><c:choose>
					<c:when test="${param.delsuccess==1}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Product Deleted !** </span>
					</c:when>
					<c:when test="${param.delsuccess==0}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Failed to Delete !** </span>
					</c:when>
				</c:choose></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>

			<td>
				<form>



					<select name="cat" id="cat" onChange="getProducts(0);">
						<option value="">Select Category</option>

						<c:forEach items="${categories}" var="cat">

							<option ${cat.getId()==catid?"selected":""}
								value="${cat.getId()}">${cat.getTitle()}</option>
						</c:forEach>




					</select>
				</form>
			</td>
			<td>
				<form>
					<select name="subcat" id="subcat" onChange="getProducts(1);">
						<option value="">Select Sub Category</option>

						<c:forEach items="${subcategories}" var="subcat">
							<option ${subcat.getId()==subcatid?"selected":""}
								value="${subcat.getId()}">${subcat.getTitle()}</option>
						</c:forEach>

					</select>
				</form>
			</td>
		</tr>
		<tr>
			<th width="10%">Serial</th>
			<th width="23%">Title</th>
			<th width="15%">Sale Price</th>
			<th width="10%">Featured</th>
			<th width="10%">Quantity</th>
			<th width="14%">Last Update</th>
			<th width="18%">ACTIONS</th>
		</tr>


		<c:forEach items="${products}" var="p">
			<tr>
				<td><%=i++ %></td>
				<td>${p.getTitle()}</td>
				<td>$${p.getSprice()}</td>
				<td>${p.getFeature()==1?"YES":"NO"}</td>
				<td>${p.getQuantity()}</td>
				<td>${p.getLastUpdate()}</td>
				<td>
					<ul class="action_link">
						<li><a href="addproducts.jsp?edit=${p.getId()}" class="edit"
							title="Edit">A</a></li>
						<li><a href="addproducts.jsp?del=${p.getId()}"
							onclick="return confirm('Are you sure ?');" class="del"
							title="Delete">A</a></li>
						<li><a href="addproducts.jsp?id=${p.getId()}&amp;move=up"
							class="up" title="Up">A</a></li>
						<li><a href="addproducts.jsp?id=${p.getId()}&amp;move=down"
							class="down" title="Down">A</a></li>
					</ul>
				</td>
			</tr>

		</c:forEach>


	</table>



</div>
<div class="clear"></div>
</div>
</div>
<script type="text/javascript">
	function getSubCat() {
		var catid = document.getElementById('category').value;
		var hid = document.getElementById('h_id').value;
		if (catid && hid)
			window.location = 'addproducts.jsp?cat=' + catid + '&edit=' + hid;
		else
			window.location = 'addproducts.jsp?cat=' + catid;
	}

	function getProducts(x) {
		var subcat, cat;
		if (x) {
			subcat = document.getElementById('subcat').value;
			cat = document.getElementById('cat').value;
			if (cat)
				window.location = 'addproducts.jsp?cat=' + cat + '&subcat='
						+ subcat;
			else if (subcat)
				window.location = 'addproducts.jsp?subcat=' + subcat;
			else
				window.location = 'addproducts.jsp';
		} else {
			cat = document.getElementById('cat').value;
			if (cat)
				window.location = 'addproducts.jsp?cat=' + cat;
			else
				window.location = 'addproducts.jsp';
		}

	}
</script>
</body>
</html>