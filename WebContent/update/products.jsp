
<jsp:include page="header.jsp" />
<%@ page
	import="com.cshop.dao.ProductManagement,com.cshop.bean.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%
	int catid = (request.getParameter("cat") != null) ? Integer.parseInt(request.getParameter("cat")) : -1;
	int subcatid = (request.getParameter("subcat") != null)
			? Integer.parseInt(request.getParameter("subcat"))
			: -1;
	int editid = (request.getParameter("edit") != null) ? Integer.parseInt(request.getParameter("edit")) : -1;

	if (catid == -1 && subcatid == -1) {

		List<Product> products = ProductManagement.getProducts(-1);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(-1);
		request.setAttribute("subcategories", subcategories);

	} else if (catid != -1 && subcatid == -1) {

		List<Product> products = ProductManagement.getProductsByCategory(catid, -1);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(catid);
		request.setAttribute("subcategories", subcategories);

	} else if (catid == -1 && subcatid != -1) {

		List<Product> products = ProductManagement.getProductsByCategory(-1, subcatid);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(-1);
		request.setAttribute("subcategories", subcategories);

	} else if (catid != -1 && subcatid != -1) {

		List<Product> products = ProductManagement.getProductsByCategory(catid, subcatid);
		request.setAttribute("products", products);

		List<SubCategory> subcategories = ProductManagement.getSubCategories(catid);
		request.setAttribute("subcategories", subcategories);

	}

	if (request.getParameter("move") != null) {
		int id = Integer.parseInt(request.getParameter("id"));

		int x = ProductManagement.sort(2, id, request.getParameter("move"));

		if (x != 0)
			response.sendRedirect("products.jsp");

	}

	if (request.getParameter("del") != null) {
		int id = Integer.parseInt(request.getParameter("del"));

		boolean x = ProductManagement.removeById(id, "g022959_products");

		if (!x)
			response.sendRedirect("products.jsp?delsuccess=1");
		else
			response.sendRedirect("products.jsp?delsuccess=0");
	}

	if (editid != -1) {

		Product product = ProductManagement.getProduct(editid);
		request.setAttribute("product", product);
	}

	List<Category> categories = ProductManagement.getCategories();
	request.setAttribute("categories", categories);

	request.setAttribute("catid", catid);
	request.setAttribute("subcatid", subcatid);
	request.setAttribute("editid", editid);

	int i = 1;
%>


<div class="rc round">
	<h2>ALL Products</h2>


	<table id="list" class="info_table" width="730px" cellpadding="0"
		cellspacing="0">

		<tr>
			<td colspan="7"><c:choose>
					<c:when test="${param.delsuccess==1}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Product Deleted !** </span>
					</c:when>
					<c:when test="${param.delsuccess==0}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Failed to Delete !** </span>
					</c:when>
				</c:choose></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>

			<td>
				<form>



					<select name="cat" id="cat" onChange="getProducts(0);">
						<option value="">Select Category</option>

						<c:forEach items="${categories}" var="cat">

							<option ${cat.getId()==catid?"selected":""}
								value="${cat.getId()}">${cat.getTitle()}</option>
						</c:forEach>




					</select>
				</form>
			</td>
			<td>
				<form>
					<select name="subcat" id="subcat" onChange="getProducts(1);">
						<option value="">Select Sub Category</option>

						<c:forEach items="${subcategories}" var="subcat">
							<option ${subcat.getId()==subcatid?"selected":""}
								value="${subcat.getId()}">${subcat.getTitle()}</option>
						</c:forEach>

					</select>
				</form>
			</td>
		</tr>
		<tr>
			<th width="10%">Serial</th>
			<th width="23%">Title</th>
			<th width="15%">Sale Price</th>
			<th width="10%">Featured</th>
			<th width="10%">Quantity</th>
			<th width="14%">Last Update</th>
			<th width="18%">ACTIONS</th>
		</tr>


		<c:forEach items="${products}" var="p">
			<tr>
				<td><%=i++ %></td>
				<td>${p.getTitle()}</td>
				<td>$${p.getSprice()}</td>
				<td>${p.getFeature()==1?"YES":"NO"}</td>
				<td>${p.getQuantity()}</td>
				<td>${p.getLastUpdate()}</td>
				<td>
					<ul class="action_link">
						<li><a href="addproducts.jsp?edit=${p.getId()}" class="edit"
							title="Edit">A</a></li>
						<li><a href="products.jsp?del=${p.getId()}"
							onclick="return confirm('Are you sure ?');" class="del"
							title="Delete">A</a></li>

						<li><a href="products.jsp?id=${p.getId()}&amp;move=up"
							class="up" title="Up">A</a></li>
						<li><a href="products.jsp?id=${p.getId()}&amp;move=down"
							class="down" title="Down">A</a></li>

					</ul>
				</td>
			</tr>

		</c:forEach>


	</table>
</div>
<div class="clear"></div>
</div>
</div>
<script type="text/javascript">
	function getProducts(x) {
		var subcat, cat;
		if (x) {
			subcat = document.getElementById('subcat').value;
			cat = document.getElementById('cat').value;
			if (cat)
				window.location = 'products.jsp?cat=' + cat + '&subcat='
						+ subcat;
			else if (subcat)
				window.location = 'products.jsp?subcat=' + subcat;
			else
				window.location = 'products.jsp';
		} else {
			cat = document.getElementById('cat').value;
			if (cat)
				window.location = 'products.jsp?cat=' + cat;
			else
				window.location = 'products.jsp';
		}

	}
</script>
</body>
</html>