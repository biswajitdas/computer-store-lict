
<jsp:include page="header.jsp" />
<%@ page
	import="com.cshop.dao.ProductManagement,com.cshop.bean.Category,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<%
	if (session.getAttribute("adminid") == null)
		response.sendRedirect("index.jsp");
%>


<c:choose>
	<c:when test="${param.edit!=null}">

		<c:set var="cid" value="${param.edit}" />
		<%
			int id = Integer.parseInt(pageContext.getAttribute("cid").toString());
					Category category = ProductManagement.getCategory(id);

					request.setAttribute("category", category);
		%>
	</c:when>

	<c:when test="${param.id!=null&&param.submit!=null}">
		<c:set var="id" value="${param.id}" />

		<%
			int id = Integer.parseInt(pageContext.getAttribute("id").toString());
					int x = ProductManagement.updateCategory(id, request.getParameter("title"));
					if (x != 0) {
						response.sendRedirect("addcat.jsp?success=2");
					}
					else if (x==0){
						response.sendRedirect("addcat.jsp?success=0");
					}
		%>

	</c:when>

	<c:when test="${param.submit!=null && param.id==null}">
		<%
			int x = ProductManagement.insertCategory(request.getParameter("title"));
		
			if(x!=0){
				response.sendRedirect("addcat.jsp?success=1");
			}
			else{
				response.sendRedirect("addcat.jsp?success=0");
			}
			
		%>
	</c:when>

</c:choose>


<%
	if (request.getParameter("del") != null) {
		int id = Integer.parseInt(request.getParameter("del"));
		boolean x = ProductManagement.removeById(id, "g022959_category");

		if (!x) {
			response.sendRedirect("addcat.jsp?delsuccess=1");
		} else {
			response.sendRedirect("addcat.jsp?delsuccess=0");
		}
	}


	if(request.getParameter("move")!=null){
		int db = Integer.parseInt(request.getParameter("db"));
		int id = Integer.parseInt(request.getParameter("id"));
		int x = ProductManagement.sort(db,id,request.getParameter("move"));


		if(x!=0){
			response.sendRedirect("addcat.jsp");
		} 
		
	}

	List<Category> list = ProductManagement.getCategories();
	request.setAttribute("list", list);
	int i=1;
%>

<div class="rc round">
	<h2>Add Category</h2>
	<form class="form_area" action="addcat.jsp" method="post">
		<table cellpadding="0" cellspacing="0" width="730px"
			class="form_table">
			<tr>
				<td colspan="2"><c:choose>
						<c:when test="${param.success==1}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**Category inserted Successfully !** </span>
						</c:when>

						<c:when test="${param.success==0}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**Sorry Try again ** </span>
						</c:when>

						<c:when test="${param.success==2}">
							<span
								style="color: red; display: block; background-color: yellow; text-align: center">
								**Category edited successfully ** </span>
						</c:when>


					</c:choose></td>
			</tr>
			<tr>
				<td width="96">Catagory Title</td>
				<c:if test="${param.edit!=null }">
					<input type="hidden" name="id" value="${category.getId()}" />
				</c:if>
				<td width="632"><input type="text" name="title"
					value="${category.getTitle()}" autofocus="autofocus" /></td>
			</tr>

			<tr>
				<td>&nbsp;</td>
				<td><input class="button" name="submit" value="Submit"
					type="submit" /> <input class="button" type="reset" /></td>
			</tr>
		</table>
	</form>


<table class="info_table" width="730px" cellpadding="0" cellspacing="0">

		<tr>
			<td colspan="2"><c:choose>
					<c:when test="${param.delsuccess==1}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Category Deleted !** </span>
					</c:when>
					<c:when test="${param.delsuccess==0}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Failed to Delete !** </span>
					</c:when>
				</c:choose>
				</td>
				</tr>
		<tr>
			<th width="11%">SL</th>
			<th width="71%">TITLE</th>
			<th width="18%">ACTIONS</th>
		</tr>




		<c:forEach items="${list}" var="l">

			<tr>
				<td><%=i++ %></td>
				<td style="text-align: left;">${l.getTitle() }</td>
				<td>
					<ul class="action_link">
						<li><a href="addcat.jsp?edit=${l.getId()}" class="edit"
							title="Edit">A</a></li>
						<li><a href="addcat.jsp?del=${l.getId() }" class="del"
							onclick="return confirm('Are you sure ?');" title="Delete">A</a></li>
						<li><a href="addcat.jsp?db=1&amp;id=${l.getId()}&amp;move=up" class="up" title="Up">A</a></li>
						<li><a href="addcat.jsp?db=1&amp;id=${l.getId()}&amp;move=down" class="down" title="Down">A</a></li>
					</ul>
				</td>
			</tr>

		</c:forEach>


	</table>
</div>
<div class="clear"></div>
</div>
</div>
</body>
</html>