<jsp:include page="header.jsp" />
<%@ page
	import="com.cshop.dao.ProductManagement,com.cshop.bean.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
	if (session.getAttribute("adminid") == null)
		response.sendRedirect("index.jsp");
%>

<%
	if (request.getParameter("del") != null) {
		int id = Integer.parseInt(request.getParameter("del"));

		boolean x = ProductManagement.removeById(id, "g022959_subcat");

		if (!x) {
			response.sendRedirect("subcat.jsp?delsuccess=1");
		} else {
			response.sendRedirect("subcat.jsp?delsuccess=0");
		}
	}

	if (request.getParameter("move") != null) {
		int db = Integer.parseInt(request.getParameter("db"));
		int id = Integer.parseInt(request.getParameter("id"));
		int x = ProductManagement.sort(db, id, request.getParameter("move"));

		if (x != 0) {
			response.sendRedirect("subcat.jsp");
		}

	}

	List<Category> categories = ProductManagement.getCategories();
	request.setAttribute("categories", categories);

	int i = 1;
	int cid = -1;
%>


<c:choose>


	<c:when test="${param.cat!=null}">
		<%
			cid = Integer.parseInt(request.getParameter("cat"));

					List<SubCategory> list = ProductManagement.getSubCategories(cid);

					request.setAttribute("subcategories", list);
					request.setAttribute("cid", cid);
		%>
	</c:when>

	<c:when test="${param.cat==null}">
		<%
			List<SubCategory> list = ProductManagement.getSubCategories(-1);

					request.setAttribute("subcategories", list);
		%>
	</c:when>
</c:choose>



<div class="rc round">
	<h2>Sub Categories</h2>

	<table class="info_table" width="730px" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="4"><c:choose>
					<c:when test="${param.delsuccess==1}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**SubCategory Deleted !** </span>
					</c:when>
					<c:when test="${param.delsuccess==0}">
						<span
							style="color: red; display: block; background-color: yellow; text-align: center">
							**Failed to Delete !** </span>
					</c:when>
				</c:choose>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<form>

					<select name="cat" id="cat" onChange="getCatagory();">
						<option value="">Show ALL</option>

						<c:forEach items="${categories}" var="c">

							<c:set value="${c.getId()}" var="tid" />

							<%
								int tid = Integer.parseInt(pageContext.getAttribute("tid").toString());
							%>

							<option <%=(tid==cid)?"selected":"" %> value="${c.getId()}">${c.getTitle()}</option>
						</c:forEach>


					</select>
				</form>
			</td>
		</tr>

		<tr>
			<th width="11%">SL</th>
			<th width="36%">Sub Category</th>
			<th width="35%">Category</th>
			<th width="18%">ACTIONS</th>
		</tr>




		<c:forEach items="${subcategories}" var="subcat">

			<c:set value="${subcat.getCategory()}" var="catid"></c:set>

			<%
				int catid = Integer.parseInt(pageContext.getAttribute("catid").toString());

					Category cat = ProductManagement.getCategory(catid);

					request.setAttribute("cat", cat);
			%>

			<tr>
				<td><%=i++%></td>
				<td style="text-align: left;">${subcat.getTitle()}</td>
				<td style="text-align: left;">${cat.getTitle()}</td>
				<td>
					<ul class="action_link">
						<li><a href="addsubcat.jsp?edit=${subcat.getId()}"
							class="edit" title="Edit">A</a></li>
						<li><a href="subcat.jsp?del=${subcat.getId()}" class="del"
							onclick="return confirm('Are you sure ?');" title="Delete">A</a></li>
						<li><a
							href="subcat.jsp?db=3&amp;id=${subcat.getId()}&amp;move=up"
							class="up" title="Up">A</a></li>
						<li><a
							href="subcat.jsp?db=3&amp;id=${subcat.getId()}&amp;move=down"
							class="down" title="Down">A</a></li>
					</ul>
				</td>
			</tr>
		</c:forEach>




	</table>
</div>
<div class="clear"></div>
</div>
</div>
<script type="text/javascript">
	function getCatagory() {
		var catid = document.getElementById('cat').value;
		//var cid = document.getElementById('cid').value;
		if (catid) {
			window.location = 'subcat.jsp?cat=' + catid;
		} else {
			window.location = 'subcat.jsp';
		}
	}
</script>
</body>
</html>