
<%@page import="com.cshop.dao.UserManagement"%>
<jsp:include page="inc/header.jsp" />

<%@ page import="com.cshop.bean.User"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>




<div class="content">
	<div class="lc">

		<h2>Sign Up</h2>
				<jsp:useBean id="bean" class="com.cshop.bean.User"></jsp:useBean>
				<jsp:setProperty property="*" name="bean" />
		<c:choose>

			<c:when test="${param.submit!=null}">
				<%
					if (UserManagement.register(bean)==1) {
								response.sendRedirect("signup.jsp?success=1");
							} else {
								response.sendRedirect("signup.jsp?success=0");
							}
				%>

			</c:when>

			<c:when test="${param.success==1}">
				<span class="notice">Signed Up Successfully. Sign in please !</span>
			</c:when>

			<c:when test="${param.success==0}">
				<span class="notice">Failed to signup. Try again!</span>
			</c:when>

		</c:choose>

		<div class="contact round">
			<form action="signup.jsp" id="contact_form" method="POST">
				<table>
					<tr>
						<td>Full Name:</td>
						<td><input type="text" name="name" required="required" /></td>
					</tr>

					<tr>
						<td>Email:</td>
						<td><input type="email" name="email" required="required" /></td>
					</tr>

					<tr>
						<td>Address:</td>
						<td><input type="text" name="address" required="required" /></td>
					</tr>

					<tr>
						<td>Username:</td>
						<td><input type="text" name="username" required="required" /></td>
					</tr>

					<tr>
						<td>Password:</td>
						<td><input type="password" name="password"
							required="required" /></td>
					</tr>

					<tr>
						<td colspan="2"><input type="submit" name="submit"
							value="Register" class="btn" /><input type="reset" value="Reset"
							class="btn" /></td>
					</tr>
				</table>
			</form>
		</div>
		<!--END contact-->
	</div>
	<!-- End of lc -->

	<jsp:include page="inc/footer.jsp" />