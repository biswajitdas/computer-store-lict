<jsp:include page="inc/header.jsp" />

<%@ page import="com.cshop.dao.*,com.cshop.bean.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
	int pid = (request.getParameter("page") != null) ? Integer.parseInt(request.getParameter("page")) : 1;
	int pageid = pid;

	int itemsPerPage = (request.getParameter("ipp") != null)
			? Integer.parseInt(request.getParameter("ipp"))
			: 6;

	if (pageid > 1) {
		pageid -= 1;
		pageid = itemsPerPage * pageid + 1;
	}

	List<Product> list, list2 = null;

	int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : -1;
	
	int subcatid = (request.getParameter("subcat") != null)
			? Integer.parseInt(request.getParameter("subcat"))
			: -1;

	int catid = (request.getParameter("cat") != null) ? Integer.parseInt(request.getParameter("cat")) : -1;

	int priceFrom = (request.getParameter("priceFrom") != null)
			? Integer.parseInt(request.getParameter("priceFrom"))
			: -1;

	int priceTo = (request.getParameter("priceTo") != null)
			? Integer.parseInt(request.getParameter("priceTo"))
			: -1;

	if (id != -1) {
		list = ProductManagement.getProducts(id);
		list2 = list;

	} else if (subcatid != -1 && catid!=-1 && priceFrom !=-1 && priceTo!=-1) {
		list2 = ProductManagement.getProductsByCategoryAndPrice(catid, subcatid,priceFrom,priceTo);

		list = ProductManagement.getProducts(pageid, itemsPerPage, catid, subcatid, priceFrom, priceTo);
		
	} else if (subcatid != -1) {
		list2 = ProductManagement.getProductsByCategory(-1, subcatid);

		list = ProductManagement.getProducts(pageid, itemsPerPage, -1, subcatid);

	} else {
		list = ProductManagement.getProducts(pageid, itemsPerPage, -1, -1);
		list2 = ProductManagement.getProducts(-1);
	}

	request.setAttribute("list", list);

	int totalItems = list2.size();

	request.setAttribute("list", list);

	int pageCount = (totalItems % itemsPerPage != 0)
			? (totalItems / itemsPerPage) + 1
			: (totalItems / itemsPerPage);

	List<Category> catlist = ProductManagement.getCategories();
	request.setAttribute("catlist", catlist);
%>


<div class="content">
	<div class="lc">
		<h2>All Products</h2>

		<div class="pagination round" style="padding: 10px">
			<form action="products.jsp" method="get">
				<span class="paginateleft" style="margin: 5px">Category : </span> <select
					class="paginateleft dropdown" style="margin: 5px"
					onchange="getSubcategories(this.value)" name="cat" required>

					<option value="0">Select Category</option>
					<c:forEach var="x" items="${catlist}">
						<option value="${x.getId()}">${x.getTitle()}</option>
					</c:forEach>

				</select> <span class="paginateleft" style="margin: 10px">Sub Category
					: </span> <select class="paginateleft dropdown" style="margin: 5px"
					id="subcategory" name="subcat" required>

					<option value="">Select SubCategory</option>


				</select>

				<div class="clear"></div>

				<span class="paginateleft" style="margin-right: 10px">Price
					From:</span> <input type="text" class="paginateleft"
					size="10" style="margin-right: 10px" name="priceFrom" required/> <span
					class="paginateleft" style="margin-right: 10px">Price To:</span>
					
					 <input
					type="text" class="paginateleft" size="10"
					style="margin-right: 10px" name="priceTo" required/> <input type="submit"
					value="Search" name="submit">

			</form>


			<div class="clear"></div>
		</div>

		<div class="pagination round" style="margin-top: 5px">
			<span class="paginateleft">Current Page : </span> <select
				class="paginateleft dropdown"
				onchange="window.location='products.jsp?subcat=<%=(subcatid != -1) ? subcatid : -1%>&page='+this[this.selectedIndex].value+'&ipp=<%=itemsPerPage%>';return false">

				<c:forEach var="x" begin="1" end="<%=pageCount%>">
					<option ${(x==param.page)?"selected":""} value="${x}">${x}</option>
				</c:forEach>

			</select> <select class="paginateright dropdown"
				onchange="window.location='products.jsp?subcat=<%=(subcatid != -1) ? subcatid : -1%>&page=1&ipp='+this[this.selectedIndex].value+'';return false">

				<option ${(param.ipp==6 || param.ipp==null)?"selected":""} value="6">6</option>
				<option ${(param.ipp==9)?"selected":""} value="9">9</option>
				<option ${(param.ipp==12)?"selected":""} value="12">12</option>

			</select> <span class="paginateright">Items per page:</span>
			<div class="clear"></div>
		</div>


		<div style="min-height: 600px">
			<c:forEach items="${list}" var="p">
				<div class="product round">
					<!-- 			<img src="update/upload/products/noimage.png" alt="Bit Diffender"
				height="140" width="150"> -->
					<img src="GetImage?id=${p.getId()}" alt="${p.getTitle()}"
						height="140" width="150">
					<div class="preview">
						<h3>
							${p.getTitle()} <span class="date">Added on
								${p.getLastUpdate()}</span> <span class="np">$${(p.getSprice()-p.getDiscount())}</span>
							<span class="price">$${p.getSprice()}</span>

						</h3>
						<p>${p.getContent()}</p>
					</div>
					<div class="clear"></div>
					<a href="mycart.jsp?oid=${p.getId()}">Add to cart</a> <a
						href="products.jsp?id=${p.getId()}">Details</a>
					<div class="clear"></div>
				</div>
			</c:forEach>
		</div>




		<div class="clear"></div>
		<div class="pagination round">
			<p class="paginate">
				Page:
				<%=pid%>
				of
				<%=pageCount%></p>
			<div class="numbs">
				<c:forEach var="x" begin="1" end="<%=pageCount%>">
					<a class="page dark ${(x==param.page || (param.page==null&&x==1))?"
						active":"gradient"}" 
					href="products.jsp?subcat=<%=(subcatid!=-1)?subcatid:-1%>&page=${x}&ipp=<%=itemsPerPage%>">${x}</a>
				</c:forEach>


			</div>
			<div class="clear"></div>
		</div>


		<script type="text/javascript">
			function getSubcategories(x) {
				var xhttp = new XMLHttpRequest();
				xhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						document.getElementById("subcategory").innerHTML = this.responseText;
					}
				};
				xhttp.open("GET", "ajax.jsp?cat=" + x, true);
				xhttp.send();
			}
		</script>


	</div>

	<jsp:include page="inc/footer.jsp" />