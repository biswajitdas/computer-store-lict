<%@page import="com.cshop.dao.*,com.cshop.bean.*,java.util.List"%>
<jsp:include page="inc/header.jsp" />

<%@ page import="com.cshop.bean.User"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<%
	int pid = (request.getParameter("page") != null) ? Integer.parseInt(request.getParameter("page")) : 1;
	int pageid = pid;

	int itemsPerPage = (request.getParameter("ipp") != null) ? Integer.parseInt(request.getParameter("ipp"))
			: 9;

	if (pageid > 1) {
		pageid -= 1;
		pageid = itemsPerPage * pageid + 1;
	}

	List<Product> list2 = ProductManagement.getFeaturedProducts();

	int totalItems = list2.size();

	List<Product> list = ProductManagement.getProducts(pageid, itemsPerPage, 1,-1);

	request.setAttribute("list", list);

	int pageCount = (totalItems % itemsPerPage != 0) ? (totalItems / itemsPerPage) + 1
			: (totalItems / itemsPerPage);
%>



<div class="content">
	<div class="lc">
		<h2>Featured Products</h2>

		<div class="pagination round">
			<span class="paginateleft">Current Page : </span><select
				class="paginateleft dropdown"
				onchange="window.location='index.jsp?page='+this[this.selectedIndex].value+'&ipp=<%=itemsPerPage%>';return false">

				<c:forEach var="x" begin="1" end="<%=pageCount%>">
					<option ${(x==param.page)?"selected":""} value="${x}">${x}</option>
				</c:forEach>

			</select> <select class="paginateright dropdown"
				onchange="window.location='index.jsp?page=1&ipp='+this[this.selectedIndex].value+'';return false">

				<option ${(param.ipp==6)?"selected":""} value="6">6</option>
				<option ${(param.ipp==9 || param.ipp==null)?"selected":""} value="9">9</option>
				<option ${(param.ipp==12)?"selected":""} value="12">12</option>

			</select> <span class="paginateright">Items per page:</span>
			<div class="clear"></div>
		</div>

		<div style="min-height:600px">
		<c:forEach items="${list}" var="p">

			<div class="img">
				<!-- <img src="update/upload/products/noimage.png" alt="Bit Diffender" width="300" height="200"> -->

				<img src="GetImage?id=${p.getId()}" alt="${p.getTitle()}"
					width="300" height="200">

				<div class="desc">
					<h3>${p.getTitle()}</h3>
					<span class="del">$${p.getSprice()}</span> <span class="ins">$${p.getSprice()-p.getDiscount()}</span>
					<a href="mycart.jsp?oid=${p.getId()}">Add to cart</a>
				</div>
			</div>
		</c:forEach>

		</div>


		<%-- 		<%
			Connection con = DBConnection.getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM products ORDER BY sorting DESC");
			while (rs.next()) {
		%>

		<div class="img">

			<img
				src="<%if (("GetImage?id=" + rs.getInt("id")) == null)
					out.println("/images/noimage.png");
				else
					out.println("GetImage?id=" + rs.getInt("id"));%>"
					
				alt="<%=rs.getString("title")%>" width="300" height="200">

			<div class="desc">
				<h3><%=rs.getString("title")%></h3>
				<span class="del"><%=rs.getInt("sprice")%></span> <span class="ins"><%=rs.getInt("sprice") - rs.getInt("discount")%></span>
				<a href="mycart.jsp?id=<%=rs.getInt("id")%>">Add to cart</a>
			</div>
		</div>

		<%
			}
		%> --%>



		<div class="clear"></div>
		<div class="pagination round">
			<p class="paginate">
				Page:
				<%=pid%>
				of
				<%=pageCount%></p>
			<div class="numbs">
				<c:forEach var="x" begin="1" end="<%=pageCount%>">
					<a class="page dark ${(x==param.page || (param.page==null&&x==1))?"active":"gradient"}" 
					href="index.jsp?page=${x}&ipp=<%=itemsPerPage%>">${x}</a>
				</c:forEach>


			</div>
			<div class="clear"></div>
		</div>
	</div>
	<!-- End of lc -->

	<jsp:include page="inc/footer.jsp" />