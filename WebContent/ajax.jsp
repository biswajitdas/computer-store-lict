<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page import="com.cshop.dao.*,com.cshop.bean.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%

if(request.getParameter("cat") != null){
	
	List<SubCategory> sublist = ProductManagement.getSubCategories(Integer.parseInt(request.getParameter("cat")));
	request.setAttribute("sublist", sublist);
}

%>

<option value="">Select SubCategory</option>
<c:forEach items="${sublist}" var="sl">
	<option value="${sl.getId()}">${sl.getTitle()}</option>
</c:forEach>

