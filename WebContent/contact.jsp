<jsp:include page="inc/header.jsp" />

<%@ page import="com.cshop.dao.*,com.cshop.bean.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="content">
	<div class="lc">

		<h2>Contact Us</h2>
		
		<c:choose>
		<c:when test="${param.success==1}">
				<span class="notice">Message sent successfully. Please wait for reply!</span>
			</c:when>

			<c:when test="${param.success==0}">
				<span class="notice">Failed to send message. Try again!</span>
			</c:when>

		</c:choose>

		<div class="contact round">

			<form action="EmailServlet" id="contact_form" method="POST">
				<table>
					<tr>
						<td>Your Name:</td>
						<td><input type="text" name="name" required="required" /></td>
					</tr>

					<tr>
						<td>Your Email:</td>
						<td><input type="email" name="email" required="required" /></td>
					</tr>

					<tr>
						<td>Subject:</td>
						<td><input type="text" name="subject" required="required" /></td>
					</tr>

					<tr>
						<td>Your Message:</td>
						<td><textarea name="message" required="required"> </textarea></td>
					</tr>

					<tr>
						<td colspan="2"><input type="submit" value="Send" class="btn" /><input
							type="reset" value="Reset" class="btn" /></td>
					</tr>
				</table>
			</form>

		</div>
		<!--END contact-->
	</div>
	<!-- End of lc -->
	<jsp:include page="inc/footer.jsp" />