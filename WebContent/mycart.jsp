<%@page import="com.cshop.dao.*,com.cshop.bean.*,java.util.List"%>
<jsp:include page="inc/header.jsp" />

<%@ page import="com.cshop.bean.User"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
	int status = -1;

	/*
	if (session.getAttribute("id") == null)
		response.sendRedirect("update/signin.jsp");
	*/
	
	if(request.getParameter("order")!=null){
		if (session.getAttribute("id") == null)
			response.sendRedirect("update/signin.jsp");
		
				
	}
	
	
	
	if (request.getParameter("oid") != null) {

		int oid = Integer.parseInt(request.getParameter("oid"));
		
		int id = (session.getAttribute("id")!=null)?Integer.parseInt(session.getAttribute("id").toString()):-1;
		
		Product product = ProductManagement.getProduct(oid);
		
		int x = ProductManagement.orderProduct(id,oid,product.getSprice()-product.getDiscount());
		
		if(x!=0){
			response.sendRedirect("mycart.jsp?success=1");
		}
		else{
			response.sendRedirect("mycart.jsp?success=0");
		}

	} else {
		List<Order> list = ProductManagement
				.getOrderes((session.getAttribute("id")!=null)?Integer.parseInt(session.getAttribute("id").toString()):-1);
		request.setAttribute("list", list);
	}

	if (request.getParameter("move") != null) {
		boolean flag = request.getParameter("move").contains("up");
		int id = Integer.parseInt(request.getParameter("id").toString());
		int pid = Integer.parseInt(request.getParameter("pid").toString());

		if (flag) {
			ProductManagement.increaseQuantity(id, pid);
		} else {
			ProductManagement.decreaseQuantity(id, pid);
		}
		response.sendRedirect("mycart.jsp");
	}

	if (request.getParameter("del") != null) {
		int id = Integer.parseInt(request.getParameter("del"));

		ProductManagement.removeById(id, "g022959_orders");
		response.sendRedirect("mycart.jsp");
	}

	int i = 1;
	int totalPrice = 0;
%>

<div class="content">
	<div class="lc">

		<h2>Products in Cart</h2>
		
		<c:choose>
			<c:when test="${param.success==1}">
			
			</c:when>
		</c:choose>

		<table class="info_table" cellpadding="0"
			cellspacing="0">
			
			
			<tr>
				<th width="10%">Serial</th>
				<th width="30%">Title</th>
				<th width="20%">Quantity</th>
				<th width="20%">Price</th>
				<th width="20%">Action</th>
			</tr>


			<c:forEach items="${list}" var="l">
				<c:set var="pid" value="${l.getPid()}"></c:set>
				<c:set var="price" value="${l.getPrice()}"></c:set>

				<%
					int pid = Integer.parseInt(pageContext.getAttribute("pid").toString());
						Product product = ProductManagement.getProduct(pid);

						totalPrice += Integer.parseInt(pageContext.getAttribute("price").toString());
						request.setAttribute("pd", product);
				%>

				<tr>
					<td><%=i++%></td>
					<td>${pd.getTitle()}</td>
					<td>${l.getQuantity()}</td>
					<td>$${l.getPrice()}</td>
					<td>
						<ul class="action_link">
							<li><a
								href="mycart.jsp?id=${l.getId()}&pid=${pd.getId()}&move=up"
								class="up" title="Up">A</a></li>
							<li><a
								href="mycart.jsp?id=${l.getId()}&pid=${pd.getId()}&move=down"
								class="down" title="Down">A</a></li>
							<li><a href="mycart.jsp?del=${l.getId()}" class="del"
								onclick="return confirm('Are you sure ?');" title="Delete">A</a>
							</li>
						</ul>
					</td>
				</tr>

			</c:forEach>

			<tr>
				<td></td>
				<td></td>
				<td>Total Price :</td>
				<td>$<%=totalPrice%></td>
				<td><button onclick="window.location='mycart.jsp?order=1'">Order</button></td>
			</tr>

		</table>

</div>

		<jsp:include page="inc/footer.jsp"></jsp:include>