package com.cshop.bean;

public class SubCategory {
	
	String title;
	int id,category,discount,distype;
	long sorting;
	public SubCategory() {
		super();
		// TODO Auto-generated constructor stub
	}
	public SubCategory(String title, int category, int discount, int distype, long sorting) {
		super();
		this.title = title;
		this.category = category;
		this.discount = discount;
		this.distype = distype;
		this.sorting = sorting;
	}
	
	
	public SubCategory(int id, String title, int category, int discount, int distype, long sorting) {
		super();
		this.title = title;
		this.id = id;
		this.category = category;
		this.discount = discount;
		this.distype = distype;
		this.sorting = sorting;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getDistype() {
		return distype;
	}
	public void setDistype(int distype) {
		this.distype = distype;
	}
	public long getSorting() {
		return sorting;
	}
	public void setSorting(long sorting) {
		this.sorting = sorting;
	}
	
	
}
