package com.cshop.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Order {
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Date date=null;
	
	int id,pid, quantity, price, cid, accepted;

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order(int pid, int quantity, int price, int cid, int accepted) {
		super();
		this.pid = pid;
		this.quantity = quantity;
		this.price = price;
		this.cid = cid;
		this.accepted = accepted;
		this.date = new Date();
	}

	public Order(int pid, int quantity, int price, int cid, Date date, int accepted) {
		super();
		this.date = date;
		this.pid = pid;
		this.quantity = quantity;
		this.price = price;
		this.cid = cid;
		this.accepted = accepted;
		this.date = date;
	}
	
	
	

	public Order(int id, int pid, int quantity, int price, int cid,Date date, int accepted) {
		super();
		this.date = date;
		this.id = id;
		this.pid = pid;
		this.quantity = quantity;
		this.price = price;
		this.cid = cid;
		this.accepted = accepted;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return dateFormat.format(date);
	}

	public void setDate() {
		this.date = new Date();
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getAccepted() {
		return accepted;
	}

	public void setAccepted(int accepted) {
		this.accepted = accepted;
	}

}
