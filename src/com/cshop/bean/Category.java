package com.cshop.bean;

public class Category {
	int id;
	String title;
	int discount,distype;
	long sorting;
	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Category(String title, int discount, int distype, long sorting) {
		super();
		this.title = title;
		this.discount = discount;
		this.distype = distype;
		this.sorting = sorting;
	}
	
	
	
	public Category(int id, String title, int discount, int distype, long sorting) {
		super();
		this.id = id;
		this.title = title;
		this.discount = discount;
		this.distype = distype;
		this.sorting = sorting;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	public int getDistype() {
		return distype;
	}
	public void setDistype(int distype) {
		this.distype = distype;
	}
	public long getSorting() {
		return sorting;
	}
	public void setSorting(long sorting) {
		this.sorting = sorting;
	}
	
	
	
}
