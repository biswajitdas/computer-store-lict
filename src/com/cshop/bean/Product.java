package com.cshop.bean;

import java.util.Calendar;
import java.util.Date;

public class Product {
	int id, sprice, feature, discount, distype, quantity, subcat, category;
	String title, content, image;
	Date lastUpdate;
	int sorting;

	public Product() {
		super();
		// TODO Auto-generated constructor stub
		lastUpdate = new Date();
		sorting = (int) Calendar.getInstance().getTimeInMillis();
	}

	public Product(int id, int sprice, int feature, int discount, int distype, int quantity, int subcat, int category,
			String title, String content, Date lastUpdate, int sorting) {
		super();
		this.id = id;
		this.sprice = sprice;
		this.feature = feature;
		this.discount = discount;
		this.distype = distype;
		this.quantity = quantity;
		this.subcat = subcat;
		this.category = category;
		this.title = title;
		this.content = content;
		this.lastUpdate = lastUpdate;
		this.sorting = sorting;
	}

	public Product(int sprice, int feature, int discount, int distype, int quantity, int subcat, int category,
			String title, String content, String image, long sorting) {
		super();
		this.sprice = sprice;
		this.feature = feature;
		this.discount = discount;
		this.distype = distype;
		this.quantity = quantity;
		this.subcat = subcat;
		this.category = category;
		this.title = title;
		this.content = content;
		this.image = image;
		this.lastUpdate = new Date();
		this.sorting = (int) Calendar.getInstance().getTimeInMillis();
	}

	public Product(int id, int sprice, int feature, int quantity, String title, Date lastUpdate) {
		super();
		this.id = id;
		this.sprice = sprice;
		this.feature = feature;
		this.quantity = quantity;
		this.title = title;
		this.lastUpdate = lastUpdate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSprice() {
		return sprice;
	}

	public void setSprice(int sprice) {
		this.sprice = sprice;
	}

	public int getFeature() {
		return feature;
	}

	public void setFeature(int feature) {
		this.feature = feature;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public int getDistype() {
		return distype;
	}

	public void setDistype(int distype) {
		this.distype = distype;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getSubcat() {
		return subcat;
	}

	public void setSubcat(int subcat) {
		this.subcat = subcat;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLastUpdate() {
		return lastUpdate.toString();
	}

	public Date getDate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public int getSorting() {
		return sorting;
	}

	public void setSorting(int sorting) {
		this.sorting = sorting;
	}

}
