package com.cshop.dao;

import static com.cshop.bean.Provider.*;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	static Connection con = null;

	public static Connection getConnection() {

		// DRIVER, CONN_URL, USER, PASS DEFINED IN com.cshop.bean.Provider INTERFACE
		if (con == null) {
			try {
				Class.forName(DRIVER);
				con = DriverManager.getConnection(CONN_URL, USER, PASS);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return con;
	}
}
