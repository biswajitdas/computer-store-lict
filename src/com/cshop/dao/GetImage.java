package com.cshop.dao;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class GetImage
 */
@WebServlet("/GetImage")
public class GetImage extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static int i;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetImage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		byte[] imaData=null;
		try {
				ServletContext sc=getServletContext();				
				Connection con=DBConnection.getConnection();				
				int id= Integer.parseInt(request.getParameter("id"));
				
				PreparedStatement st=con.prepareStatement("SELECT image FROM g022959_products WHERE id=?");
				st.setInt(1, id);
				ResultSet rs=st.executeQuery();
				while(rs.next())
				{
            	Blob image =(Blob)rs.getBlob(1);
            	imaData = image.getBytes(1, (int) image.length());
				}    
				
				
            	OutputStream output = response.getOutputStream();
            	response.setContentType("image/gif");
            	output.write(imaData);
            	
            	
            	output.flush();
            	output.close();
            	rs.close();
            	st.close();
            	
            	
           
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error in Image:"+e); 
			
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
