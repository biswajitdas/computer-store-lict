package com.cshop.dao;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.*;
import java.net.*;
 
import java.util.Properties;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 * Servlet implementation class EmailServlet
 */
@WebServlet("/EmailServlet")
@MultipartConfig(maxFileSize = 16177215) // upload file's size up to 16MB
public class EmailServlet extends HttpServlet {
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		final String err = "/contact.jsp?success=0";
		final String succ = "/contact.jsp?success=1";

		//String from = request.getParameter("from");
		String from = "cshopcontact@gmail.com";
		
		//String to = request.getParameter("to");
		String to = "biswajit.d.p@gmail.com";
		
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");
		
		String name = request.getParameter("name");
		String customerEmail = request.getParameter("email");
		
		message = "Customer Name : "+name+"\n\nCustomer Email : "+customerEmail+"\n\nMessage : "+message;
		
		//String login = request.getParameter("login");
		String login = from;
		//String password = request.getParameter("password");
		String password = "bg022959";
		
		try {
			Properties props = new Properties();
			props.setProperty("mail.host", "smtp.gmail.com");
			props.setProperty("mail.smtp.port", "587");
			props.setProperty("mail.smtp.auth", "true");
			props.setProperty("mail.smtp.starttls.enable", "true");

			Authenticator auth = new SMTPAuthenticator(login, password);

			Session session = Session.getInstance(props, auth);

			MimeMessage msg = new MimeMessage(session);
			msg.setText(message);
			msg.setSubject(subject);
			msg.setFrom(new InternetAddress(from));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			Transport.send(msg);

		} catch (AuthenticationFailedException ex) {
			request.setAttribute("ErrorMessage", "Authentication failed");

			RequestDispatcher dispatcher = request.getRequestDispatcher(err);
			dispatcher.forward(request, response);

		} catch (AddressException ex) {
			request.setAttribute("ErrorMessage", "Wrong email address");

			RequestDispatcher dispatcher = request.getRequestDispatcher(err);
			dispatcher.forward(request, response);

		} catch (MessagingException ex) {
			request.setAttribute("ErrorMessage", ex.getMessage());

			RequestDispatcher dispatcher = request.getRequestDispatcher(err);
			dispatcher.forward(request, response);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(succ);
		dispatcher.forward(request, response);

	}

	private class SMTPAuthenticator extends Authenticator {

		private PasswordAuthentication authentication;

		public SMTPAuthenticator(String login, String password) {
			authentication = new PasswordAuthentication(login, password);
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return authentication;
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}
}
