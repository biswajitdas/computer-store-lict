package com.cshop.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.cshop.bean.Category;
import com.cshop.bean.Order;
import com.cshop.bean.Product;
import com.cshop.bean.SubCategory;

public class ProductManagement {

	public static List<Category> getCategories() {
		List<Category> list = new ArrayList<Category>();

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM g022959_category ORDER BY sorting DESC");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new Category(rs.getInt("id"), rs.getString("title"), rs.getInt("discount"),
						rs.getInt("distype"), rs.getLong("sorting")));
			}
		} catch (Exception e) {
		}
		return list;
	}

	public static Category getCategory(int id) {
		Category category = null;

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM g022959_category WHERE id = ?");
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				category = new Category(rs.getInt("id"), rs.getString("title"), rs.getInt("discount"),
						rs.getInt("distype"), rs.getLong("sorting"));
			}
		} catch (Exception e) {
		}
		return category;
	}

	public static SubCategory getSubCategory(int id) {
		SubCategory subcategory = null;

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM g022959_subcat WHERE id = ? ");
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				subcategory = new SubCategory(rs.getInt("id"), rs.getString("title"), rs.getInt("category"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getLong("sorting"));
			}
		} catch (Exception e) {
		}
		return subcategory;
	}

	public static int updateCategory(int id, String title) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE g022959_category SET title=? WHERE id=?");
			ps.setString(1, title);
			ps.setInt(2, id);

			return ps.executeUpdate();

		} catch (Exception e) {
		}
		return 0;
	}

	public static int updateSubCategory(int id, int cat, String title) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE g022959_subcat SET title=?,category=? WHERE id=?");
			ps.setString(1, title);
			ps.setInt(2, cat);
			ps.setInt(3, id);

			return ps.executeUpdate();

		} catch (Exception e) {
		}
		return 0;
	}

	public static int insertCategory(String title) {
		try {
			Connection con = DBConnection.getConnection();

			PreparedStatement ps = con.prepareStatement("INSERT INTO g022959_category(title,sorting) VALUES(?,?)");

			ps.setString(1, title);
			ps.setInt(2, (int) Calendar.getInstance().getTimeInMillis());

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

	public static int insertSubCategory(int id, String title) {
		try {
			Connection con = DBConnection.getConnection();

			PreparedStatement ps = con
					.prepareStatement("INSERT INTO g022959_subcat(category,title,sorting) VALUES(?,?,?)");

			ps.setInt(1, id);
			ps.setString(2, title);
			ps.setInt(3, (int) Calendar.getInstance().getTimeInMillis());

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

	public static boolean removeById(int id, String database) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("DELETE FROM " + database + " WHERE id = ?");
			ps.setInt(1, id);
			ps.execute();

			if (database.equals("subcat")) {
				ps = con.prepareStatement("DELETE FROM g022959_products WHERE subcat=?");
				ps.setInt(1, id);
				return ps.execute();
			} else if (database.equals("category")) {
				ps = con.prepareStatement("DELETE FROM g022959_products WHERE category=?");
				ps.setInt(1, id);
				ps.execute();

				ps = con.prepareStatement("DELETE FROM g022959_subcat WHERE category=?");
				ps.setInt(1, id);
				return ps.execute();

			}

		} catch (Exception e) {
		}

		return false;

	}

	public static List<SubCategory> getSubCategories(int category) {
		List<SubCategory> list = new ArrayList<SubCategory>();

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;
			if (category == -1)
				ps = con.prepareStatement("SELECT * FROM g022959_subcat ORDER BY category ASC,sorting DESC");
			else
				ps = con.prepareStatement(
						"SELECT * FROM g022959_subcat WHERE category=" + category + " ORDER BY sorting DESC");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new SubCategory(rs.getInt("id"), rs.getString("title"), rs.getInt("category"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getLong("sorting")));

			}
		} catch (Exception e) {
		}

		return list;
	}

	public static List<Order> getOrderes(int id) {
		List<Order> list = new ArrayList<Order>();

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			if (id != -1) {
				ps = con.prepareStatement("SELECT * FROM g022959_orders WHERE cid=?");
				ps.setInt(1, id);
			}
			else if (id == -1) {
				ps = con.prepareStatement("SELECT * FROM g022959_orders WHERE cid=?");
				ps.setInt(1, id);
			}
			else {
				ps = con.prepareStatement("SELECT * FROM g022959_orders");
			}

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new Order(rs.getInt("id"), rs.getInt("pid"), rs.getInt("quantity"), rs.getInt("price"),
						rs.getInt("cid"), rs.getDate("date"), rs.getInt("accepted")));
			}

		} catch (Exception e) {
		}

		return list;
	}

	public static List<Order> getOrderes(int id, int status) {
		List<Order> list = new ArrayList<Order>();

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			if (id != -1) {
				ps = con.prepareStatement("SELECT * FROM g022959_orders WHERE cid=? and accepted=?");
				ps.setInt(1, id);
				ps.setInt(2, status);
			} else {
				ps = con.prepareStatement("SELECT * FROM g022959_orders WHERE accepted=?");
				ps.setInt(1, status);
			}
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new Order(rs.getInt("id"), rs.getInt("pid"), rs.getInt("quantity"), rs.getInt("price"),
						rs.getInt("cid"), rs.getDate("date"), rs.getInt("accepted")));
			}

		} catch (Exception e) {
		}

		return list;
	}

	public static int approve(int id, int flag) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE g022959_orders SET accepted=? WHERE id = ?");
			;
			ps.setInt(2, id);

			if (flag == 1) {
				ps.setInt(1, 1);
			} else {
				ps.setInt(1, 0);
			}

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

	public static Product getProduct(int id) {
		Product product = null;
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM g022959_products WHERE id=" + id);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				/*
				 * product = new Product(rs.getInt("id"), rs.getInt("sprice"),
				 * rs.getInt("feature"), rs.getInt("quantity"), rs.getString("title"),
				 * rs.getDate("lastupdate"));
				 */
				product = new Product(rs.getInt("id"), rs.getInt("sprice"), rs.getInt("feature"), rs.getInt("discount"),
						rs.getInt("distype"), rs.getInt("quantity"), rs.getInt("subcat"), rs.getInt("category"),
						rs.getString("title"), rs.getString("content"), rs.getDate("lastupdate"), rs.getInt("sorting"));
			}
		} catch (Exception e) {
		}
		return product;
	}

	public static List<Product> getProducts(int id) {
		List<Product> product = new ArrayList<Product>();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			if (id != -1)
				ps = con.prepareStatement("SELECT * FROM g022959_products WHERE id=" + id);
			else
				ps = con.prepareStatement("SELECT * FROM g022959_products  ORDER BY sorting DESC");

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				/*
				 * product.add(new Product(rs.getInt("id"), rs.getInt("sprice"),
				 * rs.getInt("feature"), rs.getInt("quantity"), rs.getString("title"),
				 * rs.getDate("lastupdate")));
				 */

				product.add(new Product(rs.getInt("id"), rs.getInt("sprice"), rs.getInt("feature"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getInt("quantity"), rs.getInt("subcat"),
						rs.getInt("category"), rs.getString("title"), rs.getString("content"), rs.getDate("lastupdate"),
						rs.getInt("sorting")));

			}
		} catch (Exception e) {
		}
		return product;
	}

	public static List<Product> getFeaturedProducts() {
		List<Product> product = new ArrayList<Product>();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			ps = con.prepareStatement("SELECT * FROM g022959_products WHERE feature=1 ORDER BY sorting DESC");

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				/*
				 * product.add(new Product(rs.getInt("id"), rs.getInt("sprice"),
				 * rs.getInt("feature"), rs.getInt("quantity"), rs.getString("title"),
				 * rs.getDate("lastupdate")));
				 */

				product.add(new Product(rs.getInt("id"), rs.getInt("sprice"), rs.getInt("feature"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getInt("quantity"), rs.getInt("subcat"),
						rs.getInt("category"), rs.getString("title"), rs.getString("content"), rs.getDate("lastupdate"),
						rs.getInt("sorting")));

			}
		} catch (Exception e) {
		}
		return product;
	}

	public static List<Product> getProducts(int start, int total, int featured, int subcat) {
		List<Product> product = new ArrayList<Product>();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			if (featured == -1 && subcat == -1) {
				ps = con.prepareStatement("SELECT * FROM g022959_products order by id LIMIT ?,?");
				ps.setInt(1, start - 1);
				ps.setInt(2, total);
			} else if (featured == -1 && subcat != -1) {
				ps = con.prepareStatement("SELECT * FROM g022959_products where subcat=? order by id LIMIT ?,?");
				ps.setInt(1, subcat);
				ps.setInt(2, start - 1);
				ps.setInt(3, total);
			} else {
				ps = con.prepareStatement("SELECT * FROM g022959_products where feature=? order by id LIMIT ?,?");
				ps.setInt(1, 1);
				ps.setInt(2, start - 1);
				ps.setInt(3, total);
			}

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				product.add(new Product(rs.getInt("id"), rs.getInt("sprice"), rs.getInt("feature"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getInt("quantity"), rs.getInt("subcat"),
						rs.getInt("category"), rs.getString("title"), rs.getString("content"), rs.getDate("lastupdate"),
						rs.getInt("sorting")));

			}
		} catch (Exception e) {
		}
		return product;
	}
	
	public static List<Product> getProducts(int start, int total, int cat, int subcat, int priceFrom, int priceTo) {
		List<Product> product = new ArrayList<Product>();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			
				ps = con.prepareStatement("SELECT * FROM g022959_products where category=? and subcat=? and (sprice-discount) between ? and ? order by id LIMIT ?,?");
										
				ps.setInt(1, cat);
				ps.setInt(2, subcat);
				ps.setInt(3, priceFrom);
				ps.setInt(4, priceTo);
				ps.setInt(5, start - 1);
				ps.setInt(6, total);
			

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				product.add(new Product(rs.getInt("id"), rs.getInt("sprice"), rs.getInt("feature"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getInt("quantity"), rs.getInt("subcat"),
						rs.getInt("category"), rs.getString("title"), rs.getString("content"), rs.getDate("lastupdate"),
						rs.getInt("sorting")));

			}
		} catch (Exception e) {
		}
		return product;
	}

	public static List<Product> getProductsByCategory(int cat, int subcat) {
		List<Product> product = new ArrayList<Product>();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			if (cat != -1 && subcat == -1) {
				ps = con.prepareStatement("SELECT * FROM g022959_products WHERE category=? ORDER BY sorting DESC");
				ps.setInt(1, cat);
			} else if (cat == -1 && subcat != -1) {
				ps = con.prepareStatement("SELECT * FROM g022959_products WHERE subcat=? ORDER BY sorting DESC");
				ps.setInt(1, subcat);
			} else {
				ps = con.prepareStatement(
						"SELECT * FROM g022959_products WHERE category=? and subcat=? ORDER BY sorting DESC");
				ps.setInt(1, cat);
				ps.setInt(2, subcat);
			}
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				/*
				 * product.add(new Product(rs.getInt("id"), rs.getInt("sprice"),
				 * rs.getInt("feature"), rs.getInt("quantity"), rs.getString("title"),
				 * rs.getDate("lastupdate")));
				 */

				product.add(new Product(rs.getInt("id"), rs.getInt("sprice"), rs.getInt("feature"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getInt("quantity"), rs.getInt("subcat"),
						rs.getInt("category"), rs.getString("title"), rs.getString("content"), rs.getDate("lastupdate"),
						rs.getInt("sorting")));

			}
		} catch (Exception e) {
		}
		return product;
	}

	public static List<Product> getProductsByCategoryAndPrice(int cat, int subcat, int priceFrom, int priceTo) {
		List<Product> product = new ArrayList<Product>();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;

			ps = con.prepareStatement(
					"SELECT * FROM g022959_products WHERE category=? and subcat=? and (sprice-discount) between ? and ? ORDER BY sorting DESC");
			ps.setInt(1, cat);
			ps.setInt(2, subcat);
			ps.setInt(3, priceFrom);
			ps.setInt(4, priceTo);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {

				product.add(new Product(rs.getInt("id"), rs.getInt("sprice"), rs.getInt("feature"),
						rs.getInt("discount"), rs.getInt("distype"), rs.getInt("quantity"), rs.getInt("subcat"),
						rs.getInt("category"), rs.getString("title"), rs.getString("content"), rs.getDate("lastupdate"),
						rs.getInt("sorting")));

			}
		} catch (Exception e) {
		}
		return product;
	}

	public static Order getOrder(int id) {
		Order order = null;

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM g022959_orders WHERE id=" + id);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				return new Order(rs.getInt("id"), rs.getInt("pid"), rs.getInt("quantity"), rs.getInt("price"),
						rs.getInt("cid"), rs.getDate("date"), rs.getInt("accepted"));
			}

		} catch (Exception e) {
		}

		return order;
	}

	public static void increaseQuantity(int id, int pid) {
		Product product = getProduct(pid);
		Order order = getOrder(id);

		order.setQuantity(order.getQuantity() + 1);
		order.setPrice(order.getQuantity() * product.getSprice());

		updateOrder(order);
	}

	public static void decreaseQuantity(int id, int pid) {
		Product product = getProduct(pid);
		Order order = getOrder(id);

		int qty = order.getQuantity();

		if (qty >= 1) {
			order.setQuantity(qty - 1);
			order.setPrice(order.getQuantity() * product.getSprice());

			updateOrder(order);
		}
	}

	public static void updateOrder(Order order) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE g022959_orders SET quantity=?,price=? WHERE id=?");

			ps.setInt(1, order.getQuantity());
			ps.setInt(2, order.getPrice());
			ps.setInt(3, order.getId());

			ps.execute();

		} catch (Exception e) {
		}
	}
	
	public static void updateCart(int id) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE g022959_orders SET cid=? WHERE cid=?");

			ps.setInt(1, id);
			ps.setInt(2, -1);
			
			ps.execute();

		} catch (Exception e) {
		}
	}

	public static int sort(int db, int id, String move) {
		String database = null;

		switch (db) {
		case 1:
			database = "g022959_category";
			break;
		case 2:
			database = "g022959_products";
			break;
		case 3:
			database = "g022959_subcat";
			break;
		}

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM " + database + " WHERE id=? LIMIT 0,1");

			// ps.setString(1, database);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				int sorting = rs.getInt("sorting");
				PreparedStatement ps2;
				if (move.equals("up")) {
					ps2 = con.prepareStatement(
							"SELECT * FROM " + database + " WHERE sorting >= ? ORDER BY sorting ASC LIMIT 0,2");
				} else {
					ps2 = con.prepareStatement(
							"SELECT * FROM " + database + " WHERE sorting <= ? ORDER BY sorting DESC LIMIT 0,2");
				}

				ps2.setInt(1, sorting);

				ResultSet rs2 = ps2.executeQuery();

				int sortId[] = new int[2];
				int sortOrder[] = new int[2];
				int i = 0;

				while (rs2.next()) {
					sortId[i] = rs2.getInt("id");
					sortOrder[i] = rs2.getInt("sorting");
					i++;
				}

				if (i > 1) {
					ps2 = con.prepareStatement("UPDATE " + database + " SET sorting = ? WHERE id = ? ");
					ps2.setInt(1, sortOrder[0]);
					ps2.setInt(2, sortId[1]);
					ps2.executeUpdate();

					ps2 = con.prepareStatement("UPDATE " + database + " SET sorting = ? WHERE id = ? ");
					ps2.setInt(1, sortOrder[1]);
					ps2.setInt(2, sortId[0]);
					return ps2.executeUpdate();

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static int addProduct(Product p) {

		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"INSERT INTO g022959_products(title,content,sprice,feature,discount,distype,lastupdate,quantity,sorting,subcat,category) VALUES(?,?,?,?,?,?,?,?,?,?,?)");

			ps.setString(1, p.getTitle());
			ps.setString(2, p.getContent());
			ps.setInt(3, p.getSprice());
			ps.setInt(4, p.getFeature());
			ps.setInt(5, p.getDiscount());
			ps.setInt(6, p.getDistype());
			ps.setDate(7, date);
			ps.setInt(8, p.getQuantity());
			ps.setInt(9, p.getSorting());
			ps.setInt(10, p.getSubcat());
			ps.setInt(11, p.getCategory());

			return ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static int orderProduct(int cid, int pid, int price) {
		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO g022959_orders(pid,price,cid,date) VALUES(?,?,?,?)");
			ps.setInt(1, pid);
			ps.setInt(2, price);
			ps.setInt(3, cid);
			ps.setDate(4, date);

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

}
