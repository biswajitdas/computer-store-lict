package com.cshop.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.cshop.bean.User;

public class UserManagement {

	public static int register(User u) {

		if (signin(u, 1) == 0) {
			try {
				Connection con = DBConnection.getConnection();
				PreparedStatement ps = con
						.prepareStatement("INSERT INTO g022959_user(name,email,address,username,password) values(?,?,?,?,?)");
				ps.setString(1, u.getName());
				ps.setString(2, u.getEmail());
				ps.setString(3, u.getAddress());
				ps.setString(4, u.getUsername());
				ps.setString(5, u.getPassword());

				return ps.executeUpdate();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;

	}

	public static int signin(User u, int type) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("SELECT * FROM g022959_user WHERE username=? and password=? and type=? LIMIT 1");
			ps.setString(1, u.getUsername());
			ps.setString(2, u.getPassword());
			ps.setInt(3, type);
			ResultSet rs = ps.executeQuery();

			if (rs.next())
				return rs.getInt("id");

		} catch (Exception e) {
		}

		return 0;
	}

	public static int updatePassword(int id, String oldpass, String newpass) {

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE g022959_user SET password=? WHERE id=? and password=?");

			ps.setString(1, newpass);
			ps.setInt(2, id);
			ps.setString(3, oldpass);

			return ps.executeUpdate();

		} catch (Exception e) {
			
		}

		return 0;
	}

	public static User getUser(int id) {
		User user=null;
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM g022959_user WHERE id=?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				user = new User(rs.getInt("id"), rs.getString("name"), rs.getString("email"), rs.getString("address"),
						rs.getString("username"), rs.getString("password"));
			}
		} catch (Exception e) {
		}
		return user;
	}

}
