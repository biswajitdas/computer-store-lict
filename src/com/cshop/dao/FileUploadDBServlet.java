package com.cshop.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/update/uploadServlet")
@MultipartConfig(maxFileSize = 16177215) // upload file's size up to 16MB
public class FileUploadDBServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// gets values of text fields
		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());

		int id = (Integer.parseInt(request.getParameter("id")) != 0) ? Integer.parseInt(request.getParameter("id")) : -1;

		String title = request.getParameter("title");
		String content = request.getParameter("content");
		int sprice = Integer.parseInt(request.getParameter("sprice"));
		int feature = Integer.parseInt(request.getParameter("feature"));
		int discount = Integer.parseInt(request.getParameter("discount"));
		int distype = Integer.parseInt(request.getParameter("distype"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));
		int sorting = (int) Calendar.getInstance().getTimeInMillis();
		int subcat = Integer.parseInt(request.getParameter("subcat"));
		int category = Integer.parseInt(request.getParameter("category"));

		InputStream inputStream = null; // input stream of the upload file

		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("image");
		if (filePart != null) {
			// prints out some information for debugging
			System.out.println(filePart.getName());
			System.out.println(filePart.getSize());
			System.out.println(filePart.getContentType());

			// obtains input stream of the upload file
			inputStream = filePart.getInputStream();
		}

		Connection conn = null; // connection to the database
		String message = null; // message will be sent back to client

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;

			if (id == -1) {
				ps = con.prepareStatement(
						"INSERT INTO g022959_products(title,content,sprice,feature,discount,distype,lastupdate,quantity,sorting,subcat,category,image) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)");
			} else {
				ps = con.prepareStatement("UPDATE g022959_products SET title=?,content=?,sprice=?,feature=?,discount=?,distype=?,lastupdate=?,quantity=?,sorting=?,subcat=?,category=?,image=? WHERE id=?");
				ps.setInt(13, id);
			}

			ps.setString(1, title);
			ps.setString(2, content);
			ps.setInt(3, sprice);
			ps.setInt(4, feature);
			ps.setInt(5, discount);
			ps.setInt(6, distype);
			ps.setDate(7, date);
			ps.setInt(8, quantity);
			ps.setInt(9, sorting);
			ps.setInt(10, subcat);
			ps.setInt(11, category);
			ps.setBlob(12, inputStream);

						
			if (inputStream != null) {
				// fetches input stream of the upload file for the blob column
				ps.setBlob(12, inputStream);
			}
			

			// sends the statement to the database server
			int row = ps.executeUpdate();
			if (row > 0) {
				message = "File uploaded and saved into database";
			}
		} catch (SQLException ex) {
			message = "ERROR: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				// closes the database connection
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
			// sets the message in request scope
			// request.setAttribute("Message", message);

			// forwards to the message page
			getServletContext().getRequestDispatcher("/update/addproducts.jsp?success=1").forward(request, response);
		}
	}
}